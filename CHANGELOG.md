## [1.5.1](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.5.0...v1.5.1) (2020-03-11)


### Bug Fixes

* add deprecation notice to the README.md ([6651317](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/6651317e91d4aa7b5d65b614f6b2364c7638d578))

# [1.5.0](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.4.0...v1.5.0) (2020-03-09)


### Features

* add option to suppress any other stdout or stderr output than the generated document ([83cd318](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/83cd318ee6add15e24fff1391925ad939cc6c31d))

# [1.4.0](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.3.3...v1.4.0) (2020-03-05)


### Features

* **environment:** add option to completely ignore the environment variables ([458d6e3](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/458d6e348b8edee611e3e9bd9e318e1a993b3901)), closes [#4](https://gitlab.com/blissfulreboot/javascript/env2conffile/issues/4)

## [1.3.3](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.3.2...v1.3.3) (2020-02-29)


### Bug Fixes

* **package.json:** Update the issue page and homepage of the project ([73aa8bd](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/73aa8bd103fe231c2c53d5f1c595c7eefe945e54))

## [1.3.2](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.3.1...v1.3.2) (2020-02-26)


### Bug Fixes

* **Generate:** Change the order in which the variables are added to the final env object ([2ecaaae](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/2ecaaaee5d2a187b76894086c7a783b5c0b7f944)), closes [#3](https://gitlab.com/blissfulreboot/javascript/env2conffile/issues/3)

## [1.3.1](https://github.com/XC-/env2conffile/compare/v1.3.0...v1.3.1) (2019-06-10)


### Bug Fixes

* **README:** Add corrections and adjustments to README ([ff07858](https://github.com/XC-/env2conffile/commit/ff07858))

# [1.3.0](https://github.com/XC-/env2conffile/compare/v1.2.0...v1.3.0) (2019-06-10)


### Features

* **CLI:** Support for env files. Support for configuration file. ([d399468](https://github.com/XC-/env2conffile/commit/d399468))
* **Generate:** Support for .env file ([c860bb1](https://github.com/XC-/env2conffile/commit/c860bb1))

# [1.2.0](https://github.com/XC-/env2conffile/compare/v1.1.2...v1.2.0) (2019-06-03)


### Features

* **CLI:** Imply '--output file' when defining --outputFile ([204a8fd](https://github.com/XC-/env2conffile/commit/204a8fd))

## [1.1.2](https://github.com/XC-/env2conffile/compare/v1.1.1...v1.1.2) (2019-06-02)


### Bug Fixes

* **package.json:** Fix package keywords ([b6021ca](https://github.com/XC-/env2conffile/commit/b6021ca))

## [1.1.1](https://github.com/XC-/env2conffile/compare/v1.1.0...v1.1.1) (2019-06-02)


### Bug Fixes

* **doNotStrip:** Fix momentary lapse of thought in setting the default value ([851dc1b](https://github.com/XC-/env2conffile/commit/851dc1b))

# [1.1.0](https://github.com/XC-/env2conffile/compare/v1.0.1...v1.1.0) (2019-06-02)


### Bug Fixes

* **Help:** Update help to match the latest changes ([c0ac0db](https://github.com/XC-/env2conffile/commit/c0ac0db))
* **README:** Improve README ([ac828d3](https://github.com/XC-/env2conffile/commit/ac828d3))


### Features

* **Generate:** Add option to prevent prefix and suffix stripping from the var name ([43cd082](https://github.com/XC-/env2conffile/commit/43cd082))

## [1.0.1](https://github.com/XC-/env2conffile/compare/v1.0.0...v1.0.1) (2019-06-02)


### Bug Fixes

* **installation:** Set bin script ([793f792](https://github.com/XC-/env2conffile/commit/793f792))

# 1.0.0 (2019-06-02)


### Features

* First version ([b79e1f6](https://github.com/XC-/env2conffile/commit/b79e1f6))
