const fs = require("fs");

const YAML = require("yaml");
const handlebars = require("handlebars");

const { Logger } = require("./logger");

const DEFAULT_JSON_INDENT = 2;

const errors = {
  "UNKNOWN_OUTPUT": 1,
  "UNKNOWN_RENDERER": 2,
  "NO_OUTPUT_FILE": 3,
  "NO_TEMPLATE": 4
};
const LINE_END_RE = /\r\n|\n/;

const ENVFILE_COMMENT_CHAR = "#";
const ENVFILE_VAR_SEPARATOR = "=";

function jsonPrettyPrint(msg, j) {
  Logger.log(msg, JSON.stringify(j, null, DEFAULT_JSON_INDENT));
}

function Generate(opts) {
  const outputs = {
    file: (rendered) => fs.writeFileSync(opts.outputFile, rendered, "utf-8"),
    stdout: (rendered) => console.log(rendered),
    stderr: (rendered) => console.error(rendered)
  };

  const renderers = {
    json: (obj) => JSON.stringify(obj, null, DEFAULT_JSON_INDENT),
    yaml: (obj) => YAML.stringify(obj),
    handlebars: (context) => handlebars.compile(fs.readFileSync(opts.template, "utf-8"), { strict: true })(context)
  };

  if (Object.keys(outputs).indexOf(opts.output) === -1) {
    Logger.error("Unsupported output type.");
    return process.exit(errors.UNKNOWN_OUTPUT);
  }

  if (Object.keys(renderers).indexOf(opts.renderer) === -1) {
    Logger.error("Unsupported renderer.");
    return process.exit(errors.UNKNOWN_RENDERER);
  }

  if (opts.output === "file" && !opts.outputFile) {
    Logger.error("Output file and path must be defined when using file output option.");
    return process.exit(errors.NO_OUTPUT_FILE);
  }

  if (opts.renderer === "handlebars" && !opts.template) {
    Logger.error("Path to Handlebars template must be defined when using Handlebars renderer.");
    return process.exit(errors.NO_TEMPLATE);
  }

  if (!opts.env) {
    Logger.log("Using process environment...");
    opts.env = process.env;
  }
  const filteredEnv = {};
  if (!opts.ignoreEnvironment) {
    Object.keys(opts.env)
    .filter((key) => opts.varPrefix ? key.startsWith(opts.varPrefix) : true)
    .filter((key) => opts.varSuffix ? key.endsWith(opts.varSuffix) : true)
    .sort()
    .forEach((key) => {
      let outputKey = key;
      if (!opts.doNotStrip) {
        outputKey = key.replace(new RegExp("^" + opts.varPrefix), "").replace(new RegExp(opts.varSuffix + "$"), "");
      }
      filteredEnv[outputKey] = opts.env[key]
    });
  }

  if (opts.debug) jsonPrettyPrint("Renderer context before envFile: ", filteredEnv);

  let envFromFile = {};
  if (opts.envFile) {
    fs.readFileSync(opts.envFile, "utf-8")
      .split(LINE_END_RE)
      .map((line) => line.trim())
      .filter((line) => !!line)
      .filter((line) => !line.startsWith(ENVFILE_COMMENT_CHAR))
      .map((line) => {
        let parts = line.split(ENVFILE_VAR_SEPARATOR);
        return [parts[0], parts.slice(1).join(ENVFILE_VAR_SEPARATOR)];
      })
      .forEach((pair) => envFromFile[pair[0]] = pair[1]);

      if (opts.debug) jsonPrettyPrint("Variables from envFile: ", envFromFile);
  }

  const finalEnv = Object.assign({}, envFromFile, filteredEnv);

  if (opts.debug) jsonPrettyPrint("Combined env: ", finalEnv);

  outputs[opts.output](renderers[opts.renderer](finalEnv));
}

module.exports = {
  Generate
};
