let silent = false;

module.exports = {
  Logger: {
      log: function()  {
          if (!silent) console.log(...arguments);
      },
      warn: function() {
          if (!silent) console.warn(...arguments);
      },
      error: function() {
          if (!silent) console.error(...arguments);
      },
      debug: function() {
          if (!silent) console.debug(...arguments);
      },
      setSilent: (v) => {
          if (typeof v !== "boolean") {
              console.error(`setSilent accepts only boolean values. Ignoring ${v}...`);
              return;
          }
          silent = v;
      }
  }
};
