FROM node:12-slim

RUN mkdir /env2conffile
COPY . /env2conffile/
RUN npm install -g bats
RUN npm install -g /env2conffile

WORKDIR /env2conffile/tests

ENTRYPOINT ["./e2e.bats.sh"]