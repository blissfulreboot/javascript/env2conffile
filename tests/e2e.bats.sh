#!/usr/bin/env bats

load helpers/assertions
load helpers/environmentVariables

@test "Check that --silent flag suppresses the informational output" {
  export TEST_ONELINE_SILENT="Foobar"
  run env2conffile --varPrefix TEST_ONELINE_ --renderer yaml --silent
  assertExitStatus 0
  EXPECTED=$(cat << EOM
SILENT: Foobar
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "See that JSON can be generated from environment variables by default" {
  simpleVariables
  run env2conffile --varPrefix TEST_SIMPLE_
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
{
  "FIRSTVAR": "1",
  "SECONDVAR": "2"
}
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "See that JSON can be generated from environment variables when explicitly setting the renderer" {
  simpleVariables
  run env2conffile --varPrefix TEST_SIMPLE_ --renderer json
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
{
  "FIRSTVAR": "1",
  "SECONDVAR": "2"
}
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "See that YAML can be generated from environment variables by setting renderer to yaml" {
  simpleVariables
  run env2conffile --varPrefix TEST_SIMPLE_ --renderer yaml
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
FIRSTVAR: "1"
SECONDVAR: "2"
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "See that suffix can be used to define which environment variables are to be used" {
  suffixVariables
  run env2conffile --varSuffix _SUFFIX_TEST --renderer yaml
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
FIRSTVAR: "1"
SECONDVAR: "2"
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "See that both prefix and suffix can be used to define which environment variables are to be used" {
  prefixedAndSuffixedTestVariables
  run env2conffile --varSuffix _SUFFIX --varPrefix PREFIX_ --renderer yaml
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
FIRSTVAR: "1"
SECONDVAR: "2"
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}


@test "See that Handlebars templating works with only environment variables" {
  handlebarsVariables
  run env2conffile --varPrefix TEST_HANDLEBARS_ --renderer handlebars --template static/environment.test.template.hb
  assertExitStatus 0
  EXPECTED=$(printf "Using process environment...\n$(cat static/environment.template.comparison.txt)")
  assertStringEquals "$output" "$EXPECTED"
}

@test "Dotenv-like file can be used to define variables (without prefix) and only those are rendered" {
  run env2conffile --varPrefix THISDOESNOTMATTER --envFile static/vars.env --renderer yaml
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
FILEVAR1: FILEVARVALUE1
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "Dotenv-like file can be used to define variables (without prefix) and are rendered with environment variables" {
  simpleVariables
  run env2conffile --varPrefix TEST_SIMPLE_ --envFile static/vars.env --renderer yaml
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
FILEVAR1: FILEVARVALUE1
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
FIRSTVAR: "1"
SECONDVAR: "2"
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "Dotenv-like file can be used to define variables (without prefix) and can be overridden with environment variables" {
  export OVERRIDE_FILEVAR1=9
  export OVERRIDE_FILEVAR2="foobar"
  run env2conffile --varPrefix OVERRIDE_ --envFile static/vars.env --renderer yaml
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
FILEVAR1: "9"
FILEVAR2: foobar
FileVar3: FileVarValue3
filevar4: filevarvalue4
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "Output can be written to a file (variables from file and environment) (output explicit)" {
  handlebarsVariables
  run env2conffile --varPrefix TEST_HANDLEBARS_ --envFile static/vars.env --renderer handlebars --template static/envfile.test.template.hb --output file --outputFile results.txt
  assertExitStatus 0
  GOT=$(cat results.txt)
  EXPECTED=$(cat static/envfile.template.comparison.txt)
  assertStringEquals "$GOT" "$EXPECTED"
}

@test "Output can be written to a file (variables from file and environment) (output implicit)" {
  handlebarsVariables
  run env2conffile --varPrefix TEST_HANDLEBARS_ --envFile static/vars.env --renderer handlebars --template static/envfile.test.template.hb --outputFile results.txt
  assertExitStatus 0
  GOT=$(cat results.txt)
  EXPECTED=$(cat static/envfile.template.comparison.txt)
  assertStringEquals "$GOT" "$EXPECTED"
}

@test "--ignoreEnvironment can be used to ignore the environment variables. Test 1: additional variables in environment" {
  simpleVariables
  run env2conffile --varPrefix TEST_SIMPLE_ --envFile static/vars.env --renderer yaml --ignoreEnvironment
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
FILEVAR1: FILEVARVALUE1
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "--ignoreEnvironment can be used to ignore the environment variables. Test 2: variable override" {
  export OVERRIDE_FILEVAR1=9
  export OVERRIDE_FILEVAR2="foobar"
  run env2conffile --varPrefix OVERRIDE_ --envFile static/vars.env --renderer yaml --ignoreEnvironment
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
FILEVAR1: FILEVARVALUE1
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "Fail with exit code 1 if unknown output is given (not stdout, stderr or file)" {
  run env2conffile --output foobar
  assertExitStatus 1
}

@test "Fail with exit code 2 if unknown renderer is given (not json, yaml or handlebars)" {
  run env2conffile --renderer foobar
  assertExitStatus 2
}

@test "Fail with exit code 3 if output type is file but no file is defined" {
  run env2conffile --output file
  assertExitStatus 3
}

@test "Fail with exit code 4 if handlebars renderer is used but no template given" {
  handlebarsVariables
  run env2conffile --varPrefix TEST_HANDLEBARS_ --renderer handlebars
  assertExitStatus 4
}