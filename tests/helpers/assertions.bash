assertStringEquals() {
  GOT=$1
  EXPECTED=$2
  printf "Output:\n$GOT\n\n"
  printf "Expected:\n$EXPECTED\n\n"
  [ "$GOT" == "$EXPECTED" ]
}

assertExitStatus() {
  [ $status -eq $1 ]
}