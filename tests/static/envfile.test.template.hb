This is my test template and first variable: {{FILEVAR1}}
It does not matter where the variables are, in the middle ({{FILEVAR2}}) or elsewhere.
The case should also matter: this {{FileVar3}} contains value FileVarValue3 and {{filevar4}} contains filevarvalue4.
Also those from the environment should also be added here:
{{FIRSTVAR}}
{{SECONDVAR}}
{{camelCaseVar}}
{{smallvarsmallval}}